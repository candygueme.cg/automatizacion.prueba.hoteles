#Author: Candy Guerrero Melo
@TestComplete
Feature: The user should select reservation and verify more leave

  @TestSuccesfull
  Scenario Outline: The user should select reservation and verify more leave
    Given Enter page web
    When Enter information of reservation <locate> <check> <checkOut>
    Then Verify total value

    Examples: 
      | locate          | check       | checkOut    |
      | "San Francisco" | "2/15/2020" | "2/20/2020" |
      | "Los Angeles"   | "2/10/2020" | "2/12/2020" |
      | "Las Vegas"     | "2/10/2020" | "2/12/2020" |
      | "Honolulu"      | "2/10/2020" | "2/12/2020" |
      | "Paris"         | "2/10/2020" | "2/12/2020" |
