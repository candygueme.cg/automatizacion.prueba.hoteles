package reto.usersinterfaces;

import org.openqa.selenium.By;

import java.util.List;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.core.pages.WebElementFacade;

public class ScreenReservation extends PageObject {
	public static final Target bton_city = Target.the("Button select City").located(By.xpath(
			"//*[@class='dx-first-row dx-first-col dx-last-col dx-field-item dx-col-0 dx-field-item-required dx-flex-layout dx-label-v-align']//*[@class='dx-texteditor-input']"));
	public static final Target city_select = Target.the("City selection")
			.locatedBy("//*[@class='dx-item-content dx-list-item-content'][text()='{0}']");
	public static final Target prices = Target.the("Price").located(By.xpath("//*[@class=\"rate-number\"]"));
	public static final Target page_list = Target.the("Page list").located(By.xpath("//*[@class='pagination']"));
	public static final Target prices_ok = Target.the("Price correct")
			.locatedBy("//*[@data-bind='foreach: currentHotels']/child::div[{0}]//*[@class='dx-button-content']");
	public static final Target page_selection = Target.the("Page to select")
			.locatedBy("//*[@class='external-for-plagination']//*[@class='pagination']/child::div[{0}]");

	public static final Target check = Target.the("Field de date init").located(By.xpath(
			"//*[@class='dx-first-col dx-field-item dx-col-0 dx-field-item-required dx-flex-layout dx-label-v-align']//input[contains(@id, 'checkIn')]"));
	public static final Target check_out = Target.the("Field date final").located(By.xpath(
			"//*[@class='dx-last-col dx-field-item dx-col-2 dx-field-item-required dx-flex-layout dx-label-v-align']//input[contains(@id, 'checkOut')]"));

	public static final Target btn_search = Target.the("Button search").located(By.xpath(
			"//*[@class='search main-color white-text dx-button dx-button-normal dx-button-mode-contained dx-widget dx-button-has-text']"));

	public static final Target price_total = Target.the("Field total price reservation")
			.located(By.xpath("//*[@class='total-price']"));

	public List<WebElementFacade> listPrecio() {

		List<WebElementFacade> precio = findAll(By.xpath("//*[@class='rate-number']"));

		return precio;

	}

}
