package reto.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

public class GetValueTotal implements Question<String> {
	private Target target;
	
	public GetValueTotal(Target target) {
		this.target = target;
	}
	@Override
	public String answeredBy(Actor actor) {
		return target.resolveFor(actor).getText().replaceAll("[$]", "").trim();
	}

	public static GetValueTotal theText(Target target) {
		return new GetValueTotal(target);
		
	}

}
