package reto.exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

@SuppressWarnings("serial")
public class Exceptions extends SerenityManagedException{
	public static final String MESSAGE_TIME_WAIT ="The time wait has been exceded";
	public static final String MESSAGE_ELEMNT_NOT_FOUND ="The element doesn´t found";
	public static final String MESSAGE_ELEMENT_NOT_SELECT ="Can't be clicked selected the element";
	public static final String MESSAGE_NOT_VISIBLE="The element can't be visible";
	public Exceptions(String message, Throwable testErrorException) {
		super(message, testErrorException);
	}

}
