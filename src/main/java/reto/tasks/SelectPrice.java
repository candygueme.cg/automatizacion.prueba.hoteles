package reto.tasks;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;

import net.serenitybdd.screenplay.actions.Click;
import reto.exceptions.Exceptions;
import reto.usersinterfaces.ScreenReservation;

public class SelectPrice implements Interaction {

	ScreenReservation precio;
	ScreenReservation pag;
	static int valueMin;

	@Override
	public <T extends Actor> void performAs(T actor) {

		int numCont = 0;
		int numPag = 0;
		int numPagc =0;
		valueMin = Integer.MAX_VALUE;
		try {
			List<WebElement> pages = ScreenReservation.page_list.resolveFor(actor).findElements(By.tagName("p"));
			List<WebElementFacade> cualquie;

			forP: for (int i = 0; i < pages.size(); i++) {
				cualquie = precio.listPrecio();
				for (int j = 0; j < cualquie.size(); j++) {

					if (valueMin > Integer.parseInt(cualquie.get(j).getText().replaceAll("[$]", "").trim())) {
						valueMin = Integer.parseInt(cualquie.get(j).getText().replaceAll("[$]", "").trim());
						numPagc = i;

						numCont = j;

					}

				}
				numPag++;
				if (pages.get(i) == pages.get(pages.size() - 1)) {
					break forP;

				}

				actor.attemptsTo(Click.on(ScreenReservation.page_selection.of(String.valueOf(numPag + 1))));
				
				
			}
			
			
			if ( numPag != numPagc  && pages.size() != 1) {

				actor.attemptsTo(Click.on(ScreenReservation.page_selection.of(String.valueOf(numPagc + 1))));
			}

			actor.attemptsTo(Click.on(ScreenReservation.prices_ok.of(String.valueOf(numCont + 1))));
		} catch (TimeoutException e) {
			throw new Exceptions(Exceptions.MESSAGE_TIME_WAIT, e);
		} catch (NoSuchElementException e) {
			throw new Exceptions(Exceptions.MESSAGE_ELEMENT_NOT_SELECT, e);
		} catch (ElementNotVisibleException e) {
			throw new Exceptions(Exceptions.MESSAGE_ELEMNT_NOT_FOUND, e);
		}

	}

	public static SelectPrice priceMinor() {
		return Tasks.instrumented(SelectPrice.class);

	}

	public static int getValueMin() {
		return valueMin;
	}

}
