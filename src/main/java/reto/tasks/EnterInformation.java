package reto.tasks;

import java.util.NoSuchElementException;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import reto.exceptions.Exceptions;
import reto.usersinterfaces.ScreenReservation;

public class EnterInformation implements Task {

	private String location;
	private static String check;
	private static String chekOut;

	public EnterInformation(String location, String check, String chekOut) {
		this.location = location;
		EnterInformation.check = check;
		EnterInformation.chekOut = chekOut;

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		try {
			actor.attemptsTo(WaitUntil.the(ScreenReservation.bton_city, WebElementStateMatchers.isVisible()));
			actor.attemptsTo(Click.on(ScreenReservation.bton_city));
			actor.attemptsTo(Click.on(ScreenReservation.city_select.of(String.valueOf(location))));
			actor.attemptsTo(Enter.theValue(check).into(ScreenReservation.check).thenHit(Keys.ENTER));
			actor.attemptsTo(Enter.theValue(chekOut).into(ScreenReservation.check_out));
		
			actor.attemptsTo(Click.on(ScreenReservation.btn_search));

		}catch(TimeoutException e){
			throw new Exceptions(Exceptions.MESSAGE_TIME_WAIT, e);
		}catch(NoSuchElementException e){
			throw new Exceptions(Exceptions.MESSAGE_ELEMENT_NOT_SELECT, e);
		}catch(ElementNotVisibleException e){
			throw new Exceptions(Exceptions.MESSAGE_ELEMNT_NOT_FOUND, e);
		}
		
	}

	public static EnterInformation reservation(String location, String check, String chekOut) {

		return Tasks.instrumented(EnterInformation.class, location, check, chekOut);
	}

	public static String getCheck() {
		return check;

	}

	public static String getchekOut() {
		return chekOut;

	}

}
