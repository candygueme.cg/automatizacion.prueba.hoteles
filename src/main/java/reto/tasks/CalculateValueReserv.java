package reto.tasks;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class CalculateValueReserv implements Task {
	static DecimalFormat changeFor = new DecimalFormat("0.00");
	static int valueTotal;

	@Override
	public <T extends Actor> void performAs(T actor) {
		int days = 1;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		Date dateInit;

		try {
			dateInit = dateFormat.parse(EnterInformation.getCheck());
			Date dateFinal = dateFormat.parse(EnterInformation.getchekOut());
			days = days + (int) ((dateFinal.getTime() - dateInit.getTime()) / 86400000);

			valueTotal = SelectPrice.getValueMin() * days;

		} catch (ParseException e) {

			e.printStackTrace();
		}

	}

	public static CalculateValueReserv total() {
		return Tasks.instrumented(CalculateValueReserv.class);
	}

	public static String getValueTotal() {
		return changeFor.format(valueTotal);

	}

}
