package reto.tasks;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;


import java.util.NoSuchElementException;


import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import static org.hamcrest.Matchers.equalTo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import reto.exceptions.Exceptions;
import reto.questions.GetValueTotal;
import reto.usersinterfaces.ScreenReservation;

public class VerifyValue implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		try {
			actor.attemptsTo(WaitUntil.the(ScreenReservation.price_total, WebElementStateMatchers.isVisible()));
			actor.attemptsTo(CalculateValueReserv.total());
			actor.attemptsTo(MoveMouse.to(ScreenReservation.price_total));
			actor.should(seeThat(GetValueTotal.theText(ScreenReservation.price_total), equalTo(CalculateValueReserv.getValueTotal())));

			
		} catch (TimeoutException e) {
			throw new Exceptions(Exceptions.MESSAGE_TIME_WAIT, e);
		} catch (NoSuchElementException e) {
			throw new Exceptions(Exceptions.MESSAGE_ELEMENT_NOT_SELECT, e);
		} catch (ElementNotVisibleException e) {
			throw new Exceptions(Exceptions.MESSAGE_ELEMNT_NOT_FOUND, e);
		}

	}

	public static VerifyValue Reservation() {

		return Tasks.instrumented(VerifyValue.class);
	}

}
