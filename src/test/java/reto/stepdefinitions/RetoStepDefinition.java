package reto.stepdefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import reto.tasks.EnterInformation;
import reto.tasks.SelectPrice;
import reto.tasks.VerifyValue;
import reto.utils.Url;

public class RetoStepDefinition {
	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor Catalina = Actor.named("Catalina");

	@Before
	public void configuracionInicial() {
		Catalina.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^Enter page web$")
	public void enter_page_web() {
		Catalina.wasAbleTo(Open.url(Url.urlHolel));

	}

	@When("^Enter information of reservation \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void enter_information_of_reservation(String location, String check, String chekOut) {
		Catalina.attemptsTo(EnterInformation.reservation(location, check, chekOut));
		Catalina.attemptsTo(SelectPrice.priceMinor());

	}

	@Then("^Verify total value$")
	public void verify_total_value() {
		Catalina.attemptsTo(VerifyValue.Reservation());
	}

}
